[#ftl/]
[#import "../_helpers.ftl" as helpers/]

[@helpers.html]
  [@helpers.head]
    [#-- Custom <head> code goes here --]
  [/@helpers.head]
  [@helpers.body]
    [@helpers.header]
      [#-- Custom header code goes here --]
    [/@helpers.header]

    [@helpers.appriseMain title=theme.message('forgot-password-email-sent-title')]
      <p>
        ${theme.message('forgot-password-email-sent')}
      </p>
      <div class="right return">
        [@helpers.applink /]
      </div>
    [/@helpers.appriseMain]

    [@helpers.footer]
      [#-- Custom footer code goes here --]
    [/@helpers.footer]
  [/@helpers.body]
[/@helpers.html]