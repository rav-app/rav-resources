[#ftl/]
[#import "../_helpers.ftl" as helpers/]

[@helpers.html]
  [@helpers.head]
    [#-- Custom <head> code goes here --]
  [/@helpers.head]
  [@helpers.body]
    [@helpers.header]
      [#-- Custom header code goes here --]
    [/@helpers.header]

    [@helpers.appriseMain title=theme.message('forgot-password-title')]
      <form action="forgot" method="POST" class="full">
        [@helpers.oauthHiddenFields/]

        <p>
          ${theme.message('forgot-password')}
        </p>
        <fieldset class="push-less-top">
          [@helpers.inputMaterial type="text" name="email" id="email" autocapitalize="none" autofocus=true autocomplete="on" autocorrect="off" placeholder=theme.message('email') leftAddon="user" required=true/]
        </fieldset>
        <div class="form-row">
          <div class="actions-row">
          		[@helpers.buttonMaterial text=theme.message('submit')/]
          	<div class="right return">
          		<p class="mt-2">[@helpers.applink/]</p>
          	</div>
          </div>
        </div>
      </form>
    [/@helpers.appriseMain]

    [@helpers.footer]
      [#-- Custom footer code goes here --]
    [/@helpers.footer]
  [/@helpers.body]
[/@helpers.html]