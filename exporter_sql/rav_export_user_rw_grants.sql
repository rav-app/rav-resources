GRANT USAGE ON SCHEMA rav TO olivier;
GRANT USAGE ON SCHEMA rav_public TO olivier;

GRANT SELECT, INSERT, UPDATE, DELETE ON
    rav.export_log,
    rav.record,
    rav.record_history,
    rav.record_history_delisted,
    rav.record_history_to_gear,
    rav.record_history_to_trx_authz,
    rav.record_to_gear,
    rav.record_to_trx_authz,
    rav_public.record_to_gear,
    rav_public.record,
    rav_public.record_history_to_gear,
    rav_public.record_history_delisted,
    rav_public.record_history
TO olivier;