GRANT USAGE ON SCHEMA rav TO epsm_rav;
GRANT USAGE ON SCHEMA rav_public TO epsm_rav;
GRANT USAGE ON SCHEMA rav TO mrag_rop;
GRANT USAGE ON SCHEMA rav_public TO mrag_rop;
GRANT USAGE ON SCHEMA rav TO clav;
GRANT USAGE ON SCHEMA rav_public TO clav;
GRANT USAGE ON SCHEMA rav TO rouser;
GRANT USAGE ON SCHEMA rav_public TO rouser;

GRANT SELECT ON
    rav.record,
    rav.record_to_gear,
    rav.record_history,
    rav.record_history_delisted,
    rav.record_history_to_gear
TO epsm_rav;

GRANT SELECT ON
    rav.export_log,
    rav.record,
    rav.record_history,
    rav.record_history_delisted,
    rav.record_history_to_gear,
    rav.record_to_gear,
    rav.record_history_to_trx_authz,
    rav.record_to_trx_authz
TO mrag_rop;

GRANT SELECT ON
    rav.export_log,
    rav.record,
    rav.record_history,
    rav.record_history_delisted,
    rav.record_history_to_gear,
    rav.record_to_gear,
    rav_public.record_to_gear,
    rav_public.record,
    rav_public.record_history_to_gear,
    rav_public.record_history_delisted,
    rav_public.record_history
TO clav;

GRANT SELECT ON
    rav.export_log,
    rav.record,
    rav.record_history,
    rav.record_history_delisted,
    rav.record_history_to_gear,
    rav.record_history_to_trx_authz,
    rav.record_to_gear,
    rav.record_to_trx_authz,
    rav_public.record_to_gear,
    rav_public.record,
    rav_public.record_history_to_gear,
    rav_public.record_history_delisted,
    rav_public.record_history
TO rouser;