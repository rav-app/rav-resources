import { augment, prefixed } from './common.mjs'
import { template } from './template.mjs'

export const render = (t, params = {}, original={}) => {

    const templateparams = augment({ ...params, request:original })

    const baseKey = `template.mergevessel`

    const keychain = [baseKey]

    const prefixedT = prefixed(t,templateparams, keychain )

    return template(prefixedT,templateparams)

}
