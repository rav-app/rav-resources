import { augment, prefixed } from './common.mjs'
import { template } from './template.mjs'

export const render = (t, params = {}, original={}) => {

    const [prefixedT, delistparams] = preprocess(t, params, original)

    return template(prefixedT,delistparams)

}

export const subject =  (t, params = {}, original) => {

    const [prefixedT,delistparams] = preprocess(t,params,original)

    return prefixedT('subject',delistparams)

}
 

const preprocess = (t, params = {}, original={}) => {

    const { lng, targetTenant } = params
    const { vessels } = original

    const baseKey = `template.autodelist`

    const tenancy = targetTenant? "tenant" : "notenant"

    const keychain = [`${baseKey}`, `${baseKey}.${tenancy}`]

    const delistparams = augment({ ...params, request:original})

    const prefixedT = prefixed(t,delistparams, keychain )

    const vesselUrl = uvi => `${process.env.APP_URL}/fe/record/${uvi}`

    const expired = vessels.map( ({uvi,name,code,to})=>  prefixedT(`expired.${tenancy}`, '', {
        
        vessel: `<a href='${vesselUrl(uvi)}'>${uvi} ${name}</a>`, 
        code: `<span class="emphasis">${code}</span>`,
        to:  `<span class="emphasis">${new Intl.DateTimeFormat(lng,{dateStyle:'medium'}).format(new Date(to))}</span>` }))

    const blurb =  expired.join("<br/><br/>")
   
    return [prefixedT,{...delistparams, blurb}]
}