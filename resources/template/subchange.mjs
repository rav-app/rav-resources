import { augment, prefixed } from './common.mjs'
import { template } from './template.mjs'

export const render = (t, params = {}, original={}) => {

    const [prefixedT, subchangeparams] = preprocess(t, params, original)

    return template(prefixedT,subchangeparams)

}

export const subject =  (t, params = {}, original) => {

    const [prefixedT,subchangeparams] = preprocess(t,params,original)

    return prefixedT('subject',subchangeparams)

}
 

const preprocess = (t, params = {}, original={}) => {

    const { status, rejections, targetTenant } = params

    const subchangeparams = augment({ ...params, request:original, pubnote: rejections ? t(rejections) : ''  }) 


    const baseKey = `template.subchange`
    
    const tenancy = targetTenant ? "tenant" : "notenant"

    const keychain = [
        `${baseKey}.${status}`,
        `${baseKey}.${status}.${tenancy}`,
        `${baseKey}.default`
    ]

    const prefixedT = prefixed(t,subchangeparams, keychain )

    return [prefixedT, subchangeparams]
}