import { augment, prefixed } from './common.mjs'
import { template } from './template.mjs'

export const render = (t, params = {}, original={}) => {

    const personaltestparams = augment({ ...params, request:original })

    const baseKey = `template.personaltest`

    const keychain = [baseKey]

    const prefixedT = prefixed(t,personaltestparams, keychain )

    return template(prefixedT,personaltestparams)

}
