import process from 'process'



export const linksFrom = (params = {}) => {

    const { submissionId, vesselUvi } = params



    const app_url = `${process.env.APP_URL}`
    const app_link = anchorOf(app_url, 'RAV')

    const vessel_url = vesselUvi ? `${app_url}/fe/record/${vesselUvi}` : undefined
    const submission_url = submissionId ? `${app_url}/fe/submission/${submissionId}` : app_url
    const submission_link = anchorOf(submission_url, 'Submission') //TODO : Change the hardcoded submission text with a proper translation

    // TODO....

    const action_url =  vessel_url ?? submission_url ?? app_url

    return Object.entries({

        app_url,
        app_link,

        submission_url,
        submission_link,

        action_url,

    
    }).reduce((acc, [key, val]) => ({ ...acc, [key]: val || (key.includes('_url') ? app_url : key.includes('_link') ? app_link : val) }), {})

}

export const anchorOf = (href, text) => href ? `<a href='${href}'>${text}</a>` : undefined

export const augment = (params) => {


    return {

        ...params

        ,

        ...linksFrom(params),

        interpolation: {
            escapeValue: false
        }
    }
}

// returns a proxy of the t function that prefixes keys with a chain of fallback keys (from most specific to least specific).
export const prefixed = (t, params, keychain) => (keys, dflt = '', extraparams={}) => {

    const keyArray = Array.isArray(keys) ? keys : [keys]

    const augmentedChain = [...keychain, 'template']

    const augmentedKeys = [...keyArray.flatMap(key => augmentedChain.map(chainkey => `${chainkey}.${key}`)), ...keyArray]

    return t(augmentedKeys, dflt, {...params, ...extraparams})


}


